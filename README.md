* ОСНОВНОЙ СКРИПТ ЗАПУСКА *

* Парсить аккаунты *
php parser.php parse account 1000
* Парсить посты юзеров *
php parser.php parse user-post 1000
* Парсить посты юзеров *
php parser.php parse follower 1000

- Убить все старые процессы.
- Удалить мёртвые прокси и освободить от метки использования
- Разблокировать все таблицы на запись
- Запустить парсинг процессов, где 1000 это кол-во процессов


* ЗАПУСТИТЬ ПАРСИНГ В НЕСКОЛЬКО ПРОЦЕССОВ *

* Парсить аккаунты *
php parser.php simple-run account 50
* Парсить посты юзеров *
php parser.php simple-run user-post 100

- Где 50 - кол-во процессов
- лучше используйте total-restart


* УБИТЬ ВСЕ ПРОЦЕССЫ И ОЧИСТИТЬ ТАБЛИЦЫ *

* акков *
php parser.php kill-all account
* постов юзеров *
php parser.php kill-all user-post
* фоловеров *
php parser.php kill-all follower


*УЗНАТЬ КОЛ-ВО ЗАПИСЕЙ ДОБАВЛЕННЫХ В БАЗУ*

php parser.php requests-per-last-hours account 1 1 1000
php parser.php requests-per-last-hours user-post 1 1 1000
- в примере за последний 1 час с 1 по 1000 таблицу


*УЗНАТЬ КОЛ-ВО ЗАПУЩЕНЫХ ПРОЦЕССОВ*

php parser.php get-live-process-count

* СОЗДАТЬ ТАБЛИЦЫ *

* таблицы акков *
php parser.php create-tables account 1 1000
* таблицы постов юзеров *
php parser.php create-tables user-post 1 1000
* таблицы фоловеров *
php parser.php create-tables follower 1 1000
- Первая таблица будет 'acc100', последняя 'acc20000'
- Макс имальный объём каждой таблицы 99999 записей
- Так же в каждой таблице создаёт 1 строку с id = 1, во второй таблице с 100000 и т.д.


* УДАЛИТЬ ТАБЛИЦЫ *

* таблицы акков*
php parser.php drop-tables account 1 1000
*таблицы постов юзеров*
php parser.php drop-tables user-post 1 1000


* РАЗБЛОКИРОВАТЬ ВСЕ ТАБЛИЦЫ НА ЗАПИСЬ *

*таблицы акков*
php parser.php unlock-tables account
*таблицы постов юзеров*
php parser.php unlock-tables user-post


*ЗАПУСК СКРИПТА В ФОНОВОМ РЕЖИМЕ*
nohup php /var/www/instagram-scraper-new/parser.php </dev/null &>/dev/null &

*ПОСМОТРЕТЬ ЗАПУЩЕННЫЕ PHP ПРОЦЕССЫ*
ps aux | grep "php"

N.B.
для развертывания проекта надо создать в корне файл env с таким содержимым:
dev - для тестового запуска на сервере
prod - для боевого запуска на сервере
local - для запуска на локалке
