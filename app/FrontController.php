<?php

namespace app;

use RouterException;

/**
 * @author Андрей Сердюк
 */
class FrontController
{
    const ENTITY_NAMES = [
        'account' => 'account',
        'user-post' => 'userStory',
        'follower' => 'follower',
    ];

    /**
     * @var \crawler\Factory $factory
     */
    protected $factory;
    /**
     * Аргументы командной строки
     * @var array $args
     */
    protected $args;

    /**
     * @param Factory $factory
     */
    public function __construct($args)
    {
        $this->args = $args;
        $className = "Factory";
        if (isset($args[1])) {
            if (!isset(self::ENTITY_NAMES[$args[1]])) {
                throw new RouterException('Неверный параметр #2');
            }
            $entityName = self::ENTITY_NAMES[$args[1]];
            $className = ucfirst($entityName) . $className;
        }
        $className = "\\crawler\\factories\\$className";
        /** @var \crawler\factories\Factory $factory */
        $this->factory = new $className('crawler');
    }

    /**
     * Направляет запросы по скриптам
     * 
     * @param array $args аргументы командной строки
     * @return void
     */
    public function route()
    {
        $action = array_shift($this->args);
        $actionArr = array_map(function($word) {
            return ucfirst($word);
        }, explode('-', $action));
        $action = lcfirst(implode('', $actionArr));

        array_shift($this->args);
        $this->$action($this->args);
    }

    /**
     * Основной скрипт запуска
     * 
     * @param array $args
     * @return void
     */
    private function parse($args)
    {
        $this
            ->unlockTables()
            ->factory
                ->getProxy()
                    ->deleteDead()
                    ->freeAll();

        list($processesCount) = $args;
        if (!isset($processesCount)) {
            $processesCount = 1000;
        }
        // Запустить парсинг в $processesCount процессов
        for ($i = 0; $i < $processesCount; $i++) {
            if (0 < $this->factory->getDomainModel()->getSlaveTables('free')) {
                exec("nohup php parser.php single-parser {$this->factory->getEntityName()} $i >/dev/null 2>&1 &");
                usleep(1000000);
            } else {
                error_log(print_r("All tables are in work, you can't fork parser", true));
            }
        }
    }

    /**
     * Скрипт парсера
     * @return void
     */
    private function singleParser($args)
    {
        $this->factory
            ->getParser()
            ->parse();
    }

    /**
     * Создать таблицы
     * 
     * @param array $args
     * @return void
     */
    private function createTables($args)
    {
        list($from, $to) = $args;
        if (!isset($from)) {
            $from = 1;
        }
        if (!isset($to)) {
            $to = 1000;
        }
        $this->factory->getDomainModel()->createTables($from, $to);
        echo "tables created\n";
    }

    /**
     * Удалить таблицы
     * 
     * @param array $args
     * @return void
     */
    private function dropTables($args)
    {
        list($from, $to) = $args;
        if (!isset($from)) {
            $from = 1;
        }
        if (!isset($to)) {
            $to = 1000;
        }
        $this->factory->getDomainModel()->dropTables($from, $to);
        echo "tables deleted\n";
    }

    /**
     * Разблокировать все таблицы на запись
     * 
     * @param array $args
     * @return FrontController
     */
    private function unlockTables()
    {
        $this->factory
            ->getDomainModel()
            ->unlockAllTablesForParse();
        
        return $this;
    }

    /**
     * Убить всё
     * 
     * @param array $args
     * @return void
     */
    private function killAll()
    {
        $this
            ->unlockTables()
            ->killProcesses()
            ->factory
                ->getProxy()
                    ->freeAll();
    }

    /**
     * Убить все процессы
     * 
     * @param array $args
     * @return FrontController
     */
    private function killProcesses()
    {
        $pIDs = $this->getLiveProcessIdArray();
        // ID этого процесса PHP
        $mypID = getmypid();
        if (count($pIDs) > 0) {
            foreach ($pIDs as $pID) {
                if ($pID == $mypID)
                    continue;

                exec("kill $pID");
                echo "killed $pID\n";
            }
        }
        return $this;
    }

    private function getLiveProcessIdArray()
    {
        exec("pgrep --full 'php parser.php'", $processIDs);
        return $processIDs;
    }

    /**
     * Узнать кол-во записей добавленных в базу
     * 
     * @param array $args
     * @return void
     */
    private function requestsPerLastHours($args)
    {
        list($hours, $from, $to) = $args;
        if (!isset($hours)) {
            $hours = 1;
        }
        if (!isset($from)) {
            $from = 1;
        }
        if (!isset($to)) {
            $to = 1000;
        }
        $selects = range($from, $to);
        $domainModel = $this->factory->getDomainModel();
        $selects = array_map(function($tableName) use ($domainModel) {
            return "
                SELECT *
                FROM `{$domainModel->getPrefix()}$tableName{$domainModel->getSuffix()}`
            ";
        }, $selects);

        echo "Rows parsed per $hours last hours: {$this->factory->getDb()->query('
            SELECT count(*)
            FROM (' . implode(' UNION ', $selects) . ") AS u
            WHERE u.updated_at > DATE_SUB(NOW(), INTERVAL $hours HOUR)
        ")->fetchColumn()}\n";
    }

    /**
     * Узнать кол-во запущеных процессов
     * 
     * @return void
     */
    private function getLiveProcessCount()
    {
        echo count($this->getLiveProcessIdArray()) . " processes are in work \n";
    }

    /**
     * Получить 
     * @param array $args
     * @return array
     */
    private function getItem($args)
    {
        $this->factory
            ->getParser()
            ->getItem($args[0]);
    }

    /**
     * @return void
     */
    private function test($args)
    {
        $arr = [];
        $tableNames = \crawler\helpers\ArrayHelper::flatten($this->factory->getDb()->query("
            SELECT table_name
            FROM information_schema.tables
            WHERE
                    table_schema='{$this->factory->getConfig()->getAppConf('database')['name']}'
                AND table_name LIKE 'acc%'
                AND table_name <> 'acc_master'
        ")->fetchAll(), 'table_name');
        foreach ($tableNames as $tableName) {
            $arr[] = "CREATE INDEX username ON $tableName(username);";
            //$arr[] = "DROP TABLE $tableName;";
        }
        print print_r(implode("\n", $arr), true);
    }
}
