<?php
// application configuration
return [
    'debug' => true,
    'database' => [
        'type' => 'mysql',
        'name' => 'instagram_scraper',
        'addr' => '127.0.0.1',
        'user' => 'root',
        'pass' => '',
        'charset' => 'utf8',
        'options' => [
            \PDO::ATTR_CASE => \PDO::CASE_NATURAL,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_EMULATE_PREPARES => false
        ]
    ],
    'proxy_table_name' => 'proxy_list',
    // Количество записей, которые зранятся в памяти перед сливом в БД
    'max_cached_models' => 1,
    'sleep_time_between_requests' => 60,
    // ч/з какой парсер данные получать для какой сущности
    'instagram_parsers' => [
        'account' => [
            'class_name' => 'InstagramAPI',
            'credentials' => [
                'username' => 'sych_sychov',
                'password' => 'qaswedfr',
            ],
            'truncated_debug' => false,
        ],
        'follower' => 'InstagramScraper',
        'userStory' => 'InstagramScraper',
    ],
];
