<?php
declare (strict_types = 1);

error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('log_errors', '1');
set_time_limit(9999999999);

require_once __DIR__ . '/vendor/autoload.php';

spl_autoload_register(function ($className) {
    $className = str_replace('\\', '/', $className);
    $fileName = __DIR__ . "/$className.php";
    if (file_exists($fileName)) {
        include_once($fileName);
    }
});

array_shift($argv);

(new \app\FrontController($argv))->route();
